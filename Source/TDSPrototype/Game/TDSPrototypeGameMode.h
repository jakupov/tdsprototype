// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDSPrototypeGameMode.generated.h"

UCLASS(minimalapi)
class ATDSPrototypeGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDSPrototypeGameMode();
};



