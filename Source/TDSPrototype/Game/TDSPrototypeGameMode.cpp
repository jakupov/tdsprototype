// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSPrototypeGameMode.h"
#include "TDSPrototypePlayerController.h"
#include "../Character/TDSPrototypeCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSPrototypeGameMode::ATDSPrototypeGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSPrototypePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character Blueprint'
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BPCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}