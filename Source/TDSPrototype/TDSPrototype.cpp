// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSPrototype.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDSPrototype, "TDSPrototype" );

DEFINE_LOG_CATEGORY(LogTDSPrototype)
 